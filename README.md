# Exercise Performance Tracking (EPL)  
The goal of this project is to create a database that contains performance results from exercising.   
To achive this the following points are to be met for a MVP.  
* Establish a database.  
* Make it easy to add records into the database.  
* This entire project should be able to be launched and used by any schmuck with at least an intermediate level of administrative competency.   


## Setup  
The following points are expectations for running this project and suggest usage.   
* The hosting environment should be Linux with Docker established in the environment.   
* The database should be launched via the contained docker-compose.yml file (in the docker/ dir)  
* The API is a Flask project that should run in a python virtual environment. 
* The project dependency chain is maintained via Poetry.   


