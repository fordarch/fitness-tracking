import psycopg2
import configparser
from fitness_tracking.application_configuration import Config


DB_HOST     = Config.DB_HOST
DB_NAME     = Config.DB_NAME
DB_USER     = Config.DB_USER
DB_PASSWORD = Config.DB_PASS

TABLE1 = 'motions'
TABLE2 = 'runs'
TABLE3 = 'users'

CONFIG_FILE = 'user_config.ini'


def read_table(cursor, table_name):
    cursor.execute(f'SELECT * FROM {table_name}')
    return cursor.fetchall()


def main():
    # Connect to the PostgreSQL database
    conn = psycopg2.connect(
        host=DB_HOST,
        dbname=DB_NAME,
        user=DB_USER,
        password=DB_PASSWORD
    )
    
    cursor = conn.cursor()
    
    # Read data from the tables
    data_table1 = read_table(cursor, TABLE1)
    data_table2 = read_table(cursor, TABLE2)
    data_table3 = read_table(cursor, TABLE3)
    
    # Close the cursor and connection
    cursor.close()
    conn.close()
    
    # Create a ConfigParser object
    config = configparser.ConfigParser()
    
    # Add data from each table to the configuration
    config['Table1'] = {f'row{i}': str(row) for i, row in enumerate(data_table1)}
    config['Table2'] = {f'row{i}': str(row) for i, row in enumerate(data_table2)}
    config['Table3'] = {f'row{i}': str(row) for i, row in enumerate(data_table3)}
    
    # Write the configuration to a file
    with open(CONFIG_FILE, 'w') as configfile:
        config.write(configfile)
    
    print(f'Data has been written to {CONFIG_FILE}')


if __name__ == '__main__':
    main()
