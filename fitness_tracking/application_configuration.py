import os

class Config:
    # Replace these values with your actual database credentials
    DB_NAME = os.getenv('DB_NAME', 'fitness')
    DB_USER = os.getenv('DB_USER', '')
    DB_PASS = os.getenv('DB_PASS', '')
    DB_HOST = os.getenv('DB_HOST', '192.168.1.113')
    DB_PORT = os.getenv('DB_PORT', '5432')
    
    SQLALCHEMY_DATABASE_URI = f"postgresql://{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
