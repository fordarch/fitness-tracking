from flask import Flask, render_template, request, jsonify
from waitress import serve
from flask_sqlalchemy import SQLAlchemy
from fitness_tracking.application_configuration import Config
from datetime import datetime
from sqlalchemy.dialects.postgresql import UUID
import uuid


app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)


class Selection(db.Model):
    __tablename__ = 'lifts' 

    id              = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    lift_date       = db.Column(db.DateTime, default=datetime.now(), nullable=False)
    motion_id       = db.Column(UUID(as_uuid=True), default=uuid.uuid4, nullable=False)
    lift_reps       = db.Column(db.Integer, nullable=False)
    lift_sets       = db.Column(db.Integer, nullable=False)
    lift_weight_kg  = db.Column(db.Integer, nullable=False)
    user_id         = db.Column(UUID(as_uuid=True), nullable=False)


    def __init__(self, motion_id, lift_reps, lift_sets, lift_weight_kg):
        self.motion_id = '8ceecc73-04a0-4f2a-8d85-afc4f25bf21b' #motion_id
        self.lift_reps = int(lift_reps[:-4])
        self.lift_sets = int(lift_sets[:-4])
        self.lift_weight_kg = int(lift_weight_kg[:-2])
        #self.user_id = uuid.UUID('7ec3c294-3867-49b2-b152-7a5c35071ceb').hex # default to me, for now
        self.user_id = uuid.UUID('b61881c7-2636-4337-aa22-89c3d4a88b29').hex


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/submit', methods=['POST'])
def submit():
    data = request.json
    selections = data.get('selections', [])
    print(selections)

    if len(selections) > 0:
        # Assuming selections are in the form: "value1, value2, value3"
        selection_objects = []
        for selection in selections:
            values = selection.split(', ')
            if len(values) == 4:  # Ensure there are exactly 3 values
                print(f"Preparing to insert: column1={values[0]}, column2={values[1]}, column3={values[2]}, column4={values[3]}")
                selection_objects.append(Selection(motion_id=values[0], lift_sets=values[1], lift_reps=values[2], lift_weight_kg=values[3]))
        
        # Log all objects to be inserted
        print("All selections to be inserted:", selection_objects)
        
        db.session.bulk_save_objects(selection_objects)
        db.session.commit()

        return jsonify({'status': 'success'}), 201
    else:
        return jsonify({'status': 'error', 'message': 'No selections provided'}), 400


if __name__ == '__main__':
    with app.app_context():
        db.create_all()

    #app.run(debug=True)
    serve(app, host='0.0.0.0', port=5000, threads=4)
