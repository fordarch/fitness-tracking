document.addEventListener('DOMContentLoaded', function () {
    const boxes = document.querySelectorAll('.box');
    const aggregateButton = document.getElementById('aggregateButton');
    const submitButton = document.getElementById('submitButton');
    const clearButton = document.getElementById('clearButton');
    const aggregatedSelections = document.getElementById('aggregatedSelections');

    boxes.forEach(box => {
        box.addEventListener('click', () => {
            const column = box.getAttribute('data-column');
            const selectedBoxesInColumn = document.querySelectorAll(`.box[data-column="${column}"].selected`);

            // Deselect any other selected boxes in the same column
            selectedBoxesInColumn.forEach(selectedBox => {
                selectedBox.classList.remove('selected');
            });

            // Select the clicked box
            box.classList.add('selected');
        });
    });

    aggregateButton.addEventListener('click', () => {
        const selectedBoxes = document.querySelectorAll('.box.selected');
        const selections = [];

        selectedBoxes.forEach(box => {
            selections.push(box.getAttribute('data-value'));
        });

        if (selections.length > 0) {
            const csv = selections.join(', ')

            const a = selections[0] + ", ";
            const b = selections[1] + " Sets, ";
            const c = selections[2] + " Reps, ";
            const d = selections[3] + selections[4] + selections[5] + "kg";
            const selectionText = a + b + c + d;
            //const selectionText = lastThree;

            // Create list item element
            const li = document.createElement('li');

            // Create text node for the selection
            const textNode = document.createTextNode(selectionText);

            // Create remove button
            const removeButton = document.createElement('button');
            removeButton.textContent = 'x';
            removeButton.classList.add('remove-btn');

            // Append text node and remove button to the list item
            li.appendChild(textNode);
            li.appendChild(removeButton);

            // Append the list item to the aggregated selections list
            aggregatedSelections.appendChild(li);

            console.log('Aggregated Selections:', selections);

            // Add event listener to remove the list item when the remove button is clicked
            removeButton.addEventListener('click', () => {
                aggregatedSelections.removeChild(li);
            });
        }
    });

    submitButton.addEventListener('click', () => {
        const listItems = document.querySelectorAll('#aggregatedSelections li');
        const selections = [];

        listItems.forEach(item => {
            selections.push(item.firstChild.textContent);
        });

        fetch('/submit', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ selections })
        })
        .then(response => response.json())
        .then(data => {
            if (data.status === 'success') {
                alert('Selections submitted successfully!');

                // Clear the aggregated selections list
                while (aggregatedSelections.firstChild) {
                    aggregatedSelections.removeChild(aggregatedSelections.firstChild);
                }
            }
        });
    });

    // For clearing the selection off the boxes
    clearButton.addEventListener('click', () => {
        const selectedBoxes = document.querySelectorAll('.box.selected');
        selectedBoxes.forEach(box => {
            box.classList.remove('selected');
        });
    }); 

});
