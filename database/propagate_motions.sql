SELECT *
FROM motion m 
;

INSERT INTO motion (motion_name, motion_variant) VALUES
	('Bench Press', null),
	('Squat', null),
	('Deadlift', null),
	('Deadlift', 'Banded'),
	('Deadlift', 'Deficit'),
	('Deadlift', 'Dumbbell'),
	('Deadlift', 'Eccentric Emphasis'),
	('Deadlift', 'Isometric'),
	('Deadlift', 'Partial'),
	('Deadlift', 'Single Leg'),
	('Deadlift', 'Stiff Leg'),
	('Deadlift', 'With Pause')
;